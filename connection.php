<?php
/**
 * Ariana ERP database connection
 */
$arianaHost = "localhost";
$arianaUsername = "root";
$arianaPassword = "dg247";
$arianaDB = "ariana";
$arianaPort = 33069;

/**
 * ASL database connection
 */
$aslHost = "localhost";
$aslUsername = "root";
$aslPassword = "dg247";
$aslDB = "asl";
$aslPort = 33069;

try {
    $connAriana = new PDO("mysql:host=$arianaHost:$arianaPort;dbname=$arianaDB", $arianaUsername, $arianaPassword);
    $connAsl = new PDO("mysql:host=$aslHost:$aslPort;dbname=$aslDB", $aslUsername, $aslPassword);
    // set the PDO error mode to exception
    $connAriana->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connAsl->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "DB Connected successfully.\n";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}