<?php

require_once 'connection.php';

try {
    echo "Migration started.\n";
    // Delete duplicate data from temporary tables
    $connAsl->exec("DELETE FROM tmp_vehicle_export WHERE vehicle_id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_towing_request WHERE id IN(select tmp_vehicle.towing_request_id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_export WHERE id NOT IN(SELECT export_id FROM tmp_vehicle_export);
    DELETE FROM tmp_vehicle_features WHERE vehicle_id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_images WHERE vehicle_id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_documents WHERE vehicle_id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_vehicle_features WHERE vehicle_id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_vehicle_condition WHERE vehicle_id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);
    DELETE FROM tmp_export_images WHERE export_id NOT IN(select id from tmp_export);
    DELETE FROM tmp_vehicle WHERE id IN(select tmp_vehicle.id from tmp_vehicle join vehicle v on tmp_vehicle.vin = v.vin);");
    echo "Duplicated data deleted successfully from tmp tables.\n";

    // Migrate data for towing_request
    echo "Migration started for towing request.\n";
    $sql = "SELECT * FROM tmp_towing_request WHERE asl_id IS NULL;";
    $insetSql = "INSERT INTO towing_request (`condition`, `damaged`, `pictures`, `towed`, `title_recieved`, `title_recieved_date`, `title_number`, `title_state`, `towing_request_date`, `pickup_date`, `deliver_date`, `note`, `title_type`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt = $connAsl->prepare($insetSql);
    $count = 0;
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['condition'], $row['damaged'], $row['pictures'], $row['towed'], empty(trim($row['title_recieved'])) ? 0 : trim($row['title_recieved']), $row['title_recieved_date'], $row['title_number'], $row['title_state'], $row['towing_request_date'], $row['pickup_date'], $row['deliver_date'], $row['note'], $row['title_type']]);
        $connAsl->exec("UPDATE tmp_towing_request SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s towing request successfully migrated to ASL.\r", ++$count);
    }
    printf("%s towing request successfully migrated to ASL.\n\n", $count);
    
    // Migrate data for vehicle
    echo "Migration started for vehicle.\n";
    $connAsl->exec("UPDATE tmp_vehicle SET customer_user_id = CASE WHEN customer_user_id = 7000355 THEN 7000474
        WHEN customer_user_id = 7000360 THEN 7000477
        WHEN customer_user_id = 900511 THEN 7000453
        WHEN customer_user_id = 7000313 THEN 7000437
        WHEN customer_user_id = 7000363 THEN 7000479
        WHEN customer_user_id = 7000365 THEN 7000478
        WHEN customer_user_id = 7000359 THEN 7000475
        WHEN customer_user_id = 900855 THEN 7000267
        WHEN customer_user_id = 900711 THEN 7000293
        WHEN customer_user_id = 900737 THEN 7000310
        WHEN customer_user_id = 900796 THEN 7000266
        ELSE customer_user_id = customer_user_id END;");
    $connAsl->exec("UPDATE tmp_vehicle SET location = 4 WHERE location = 5; UPDATE tmp_vehicle SET location = 5 WHERE location = 6;");
    $sql = "SELECT tmp_vehicle.*, ttr.asl_id as ttr_asl_id from tmp_vehicle join tmp_towing_request ttr on tmp_vehicle.towing_request_id = ttr.id WHERE tmp_vehicle.asl_id IS NULL;";
    $insetSql = "INSERT INTO vehicle (`hat_number`, `year`, `color`, `model`, `make`, `vin`, `weight`, `pieces`, `value`, `license_number`, `towed_from`, `lot_number`, `towed_amount`, `storage_amount`, `status`, `check_number`, `additional_charges`, `location`, `customer_user_id`, `towing_request_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `is_export`, `title_amount`, `container_number`, `keys`, `vehicle_is_deleted`, `notes_status`, `added_by_role`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['hat_number'], $row['year'], $row['color'], $row['model'], $row['make'], $row['vin'], $row['weight'], $row['pieces'], $row['value'], $row['license_number'], $row['towed_from'], $row['lot_number'], $row['towed_amount'], empty($row['storage_amount']) ? 0 : $row['storage_amount'], $row['status'], empty($row['check_number']) ? 0 : $row['check_number'], $row['additional_charges'], $row['location'], $row['customer_user_id'], $row['ttr_asl_id'], $row['created_at'], $row['updated_at'], $row['created_by'], $row['updated_by'], $row['is_export'], $row['title_amount'], $row['container_number'], empty(trim($row['keys'])) ? 0 : $row['keys'], $row['vehicle_is_deleted'], $row['notes_status'], $row['added_by_role']]);
        $connAsl->exec("UPDATE tmp_vehicle SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s vehicle successfully migrated to ASL.\r", ++$count);
    }
    printf("%s vehicle successfully migrated to ASL.\n\n", $count);

    // Migrate export data
    echo "Migration started for export.\n";
    $connAsl->exec("UPDATE tmp_export SET customer_user_id = CASE WHEN customer_user_id = 900855 THEN 7000267
        WHEN customer_user_id = 900796 THEN 7000266
        WHEN customer_user_id = 900679 THEN 7000335
        WHEN customer_user_id = 900765 THEN 7000263
        WHEN customer_user_id = 7000245 THEN 7000380
        WHEN customer_user_id = 7000167 THEN 7000371
        WHEN customer_user_id = 900635 THEN 7000487
        ELSE customer_user_id = customer_user_id END");
    $count = 0;
    $sql = "SELECT * from tmp_export WHERE asl_id IS NULL;";
    $insetSql = "INSERT INTO export (export_date, loading_date, broker_name, booking_number, eta, ar_number, xtn_number, seal_number, container_number, cutt_off, vessel, voyage, terminal, streamship_line, destination, itn, contact_details, special_instruction, container_type, port_of_loading, port_of_discharge, export_invoice, bol_note, export_is_deleted, created_by, updated_by, created_at, updated_at, legacy_customer_id, added_by_role, customer_user_id, notes_status, oti_number) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['export_date'], $row['loading_date'], $row['broker_name'], $row['booking_number'], $row['eta'], $row['ar_number'], $row['xtn_number'], $row['seal_number'], $row['container_number'], $row['cutt_off'], $row['vessel'], $row['voyage'], $row['terminal'], $row['streamship_line'], $row['destination'], $row['itn'], $row['contact_details'], $row['special_instruction'], $row['container_type'], $row['port_of_loading'], $row['port_of_discharge'], $row['export_invoice'], $row['bol_note'], $row['export_is_deleted'], $row['created_by'], $row['updated_by'], $row['created_at'], $row['updated_at'], empty(trim($row['legacy_customer_id'])) ? null : $row['legacy_customer_id'], $row['added_by_role'], $row['customer_user_id'], $row['notes_status'], $row['oti_number']]);
        $connAsl->exec("UPDATE tmp_export SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s export successfully migrated to ASL.\r", ++$count);
    }
    printf("%s export successfully migrated to ASL.\n\n", $count);
    
    // Migrate vehicle export
    echo "VehicleExport migration started.\n";
    $connAsl->exec("UPDATE tmp_vehicle_export SET customer_user_id = CASE WHEN customer_user_id = 7000355 THEN 7000474
        WHEN customer_user_id = 7000360 THEN 7000477
        WHEN customer_user_id = 900511 THEN 7000453
        WHEN customer_user_id = 7000313 THEN 7000437
        WHEN customer_user_id = 7000363 THEN 7000479
        WHEN customer_user_id = 7000365 THEN 7000478
        WHEN customer_user_id = 7000359 THEN 7000475
        WHEN customer_user_id = 900855 THEN 7000267
        WHEN customer_user_id = 900711 THEN 7000293
        WHEN customer_user_id = 900737 THEN 7000310
        WHEN customer_user_id = 900796 THEN 7000266
        ELSE customer_user_id = customer_user_id END;");
    $count = 0;
    $sql = "select tmp_vehicle_export.*, v.asl_id as tmp_vehicle_id, e.asl_id as tmp_export_id
    from tmp_vehicle_export
             join tmp_vehicle v on tmp_vehicle_export.vehicle_id = v.id
             join tmp_export e on tmp_vehicle_export.export_id = e.id
    GROUP BY tmp_vehicle_export.id;";
    $insetSql = "INSERT INTO tmp_vehicle_export (vehicle_id,export_id,customer_user_id,custom_duty,clearance,towing,shipping,storage,local,others,additional,vat,remarks,title,discount,vehicle_export_is_deleted,notes_status,exchange_rate,ocean_charges) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['tmp_vehicle_id'], $row['tmp_export_id'], $row['customer_user_id'], $row['custom_duty'], $row['clearance'], $row['towing'], $row['shipping'], $row['storage'], $row['local'], $row['others'], $row['additional'], $row['vat'], $row['remarks'], $row['title'], $row['discount'], $row['vehicle_export_is_deleted'], $row['notes_status'], $row['exchange_rate'], $row['ocean_charges']]);
        $connAsl->exec("UPDATE tmp_vehicle_export SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s vehicle_export successfully migrated to ASL.\r", ++$count);
    }
    printf("%s vehicle_export successfully migrated to ASL.\n\n", $count);

    // Migrate images
    echo "Images Migration started.\n";
    $sql = "select tmp_images.*, v.asl_id as tmp_vehicle_id from tmp_images join tmp_vehicle v on tmp_images.vehicle_id = v.id;";
    $insetSql = "INSERT INTO images (`name`, `thumbnail`, `normal`, `vehicle_id`, `is_deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`, `baseurl`) VALUES(?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    $count = 0;
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['name'], $row['thumbnail'], $row['normal'], $row['tmp_vehicle_id'], $row['is_deleted'], $row['created_at'], $row['updated_at'], $row['created_by'], $row['updated_by'], $row['baseurl']]);
        $connAsl->exec("UPDATE tmp_images SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s images successfully migrated to ASL.\r", ++$count);
    }
    printf("%s images successfully migrated to ASL.\n\n", $count);
    
    // Migrate export_images
    echo "Export Images migration started.\n";
    $sql = "select tmp_export_images.*, e.asl_id as tmp_export_id from tmp_export_images join tmp_export e on tmp_export_images.export_id = e.id;";
    $insetSql = "INSERT INTO tmp_export_images (`name`, `thumbnail`, `export_id`, `baseurl`) VALUES (?,?,?,?)";
    $stmt = $connAsl->prepare($insetSql);
    $count = 0;
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['name'], $row['thumbnail'], $row['tmp_export_id'], $row['baseurl']]);
        $connAsl->exec("UPDATE tmp_export_images SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s export_images successfully migrated to ASL.\r", ++$count);
    }
    printf("%s export_images successfully migrated to ASL.\n\n", $count);

    // Migrate vehicle_condition
    echo "VehicleCondition migration started.\n";
    $sql = "SELECT tmp_vehicle_condition.*, v.asl_id as tmp_vehicle_id, c.asl_id as tmp_condition_id FROM tmp_vehicle_condition join `tmp_condition` c on tmp_vehicle_condition.condition_id = c.id join tmp_vehicle v on tmp_vehicle_condition.vehicle_id = v.id;";
    $insetSql = "INSERT INTO vehicle_condition (`value`, `vehicle_id`, `condition_id`) VALUES (?,?,?)";
    $count = 0;
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['value'], $row['tmp_vehicle_id'], $row['tmp_condition_id']]);
        $connAsl->exec("UPDATE tmp_vehicle_condition SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s vehicle_condition successfully migrated to ASL.\r", ++$count);
    }
    printf("%s vehicle_condition successfully migrated to ASL.\n\n", $count);
    
    // Migrate vehicle_features
    echo "vehicle_features migration started.\n";
    $sql = "SELECT tmp_vehicle_features.*, v.asl_id as tmp_vehicle_id, c.asl_id as tmp_features_id FROM tmp_vehicle_features join `tmp_features` c on tmp_vehicle_features.features_id = c.id join tmp_vehicle v on tmp_vehicle_features.vehicle_id = v.id;";
    $count = 0;
    $insetSql = "INSERT INTO vehicle_features (`vehicle_id`, `features_id`, `value`) VALUES (?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['tmp_vehicle_id'], $row['tmp_features_id'], $row['value']]);
        $connAsl->exec("UPDATE tmp_vehicle_features SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s vehicle_feature successfully migrated to ASL.\r", ++$count);
    }
    printf("%s vehicle_feature successfully migrated to ASL.\n\n", $count);

    // Migrate documents
    echo "documents migration started.\n";
    $sql = "SELECT tmp_documents.*, v.asl_id as tmp_vehicle_id FROM tmp_documents join tmp_vehicle v on tmp_documents.vehicle_id = v.id;";
    $count = 0;
    $insetSql = "INSERT INTO documents (`name`, `thumbnail`, `normal`, `is_deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`, `vehicle_id`, `invoice_id`) VALUES (?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAsl->query($sql) as $row) {
        $stmt->execute([$row['name'], $row['thumbnail'], $row['normal'], $row['is_deleted'], $row['created_at'], $row['updated_at'], $row['created_by'], $row['updated_by'], $row['tmp_vehicle_id'], $row['invoice_id']]);
        $connAsl->exec("UPDATE tmp_documents SET asl_id = ".$connAsl->lastInsertId()." WHERE id = ".$row['id']);
        printf("%s documents successfully migrated to ASL.\r", ++$count);
    }
    printf("%s documents successfully migrated to ASL.\n\n", $count);
    
} catch (Exception $e) {
    echo $e->getMessage();
}