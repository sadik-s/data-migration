<?php
require_once 'connection.php';

try {
    // Migrate condition data to tmp_condition table
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_condition`; CREATE TABLE `tmp_condition`( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(45) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = 'SELECT * FROM `condition`;';
    $insetSql = "INSERT INTO tmp_condition (id, name) VALUES (?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['name']]);
    }
    $connAsl->exec("UPDATE `tmp_condition` LEFT JOIN `condition` ON `tmp_condition`.name = `condition`.name SET `tmp_condition`.asl_id = `condition`.id;");
    echo "condition data added to tmp_condition table.\n";

    // Migrate vehicle_condition data to tmp_vehicle_condition table
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_vehicle_condition`; CREATE TABLE `tmp_vehicle_condition`( `id` int(11) NOT NULL AUTO_INCREMENT, `value` varchar(45) DEFAULT NULL, `vehicle_id` int(11) NOT NULL, `condition_id` int(11) NOT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select vc.* from vehicle join vehicle_condition vc on vc.vehicle_id = vehicle.id where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_vehicle_condition (`id`, `value`, `vehicle_id`, `condition_id`) VALUES (?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['value'], $row['vehicle_id'], $row['condition_id']]);
    }
    echo "vehicle_condition data added to tmp_vehicle_condition table.\n";

    // Migrate Export data to tmp_export table
    $connAsl->exec("ALTER TABLE export MODIFY COLUMN export_is_deleted tinyint(1) DEFAULT '0';");
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_export`; CREATE TABLE `tmp_export`( `id` int(11) NOT NULL AUTO_INCREMENT, `export_date` date DEFAULT NULL, `loading_date` date DEFAULT NULL, `broker_name` varchar(450) DEFAULT NULL, `booking_number` varchar(45) DEFAULT NULL, `eta` date DEFAULT NULL, `ar_number` varchar(45) DEFAULT NULL, `xtn_number` varchar(450) DEFAULT NULL, `seal_number` varchar(45) DEFAULT NULL, `container_number` varchar(45) DEFAULT NULL, `cutt_off` date DEFAULT NULL, `vessel` varchar(45) DEFAULT NULL, `voyage` varchar(45) DEFAULT NULL, `terminal` varchar(45) DEFAULT NULL, `streamship_line` varchar(200) DEFAULT NULL, `destination` varchar(256) DEFAULT NULL, `itn` varchar(256) DEFAULT NULL, `contact_details` text DEFAULT NULL, `special_instruction` text DEFAULT NULL, `container_type` varchar(45) DEFAULT NULL, `port_of_loading` varchar(200) DEFAULT NULL, `port_of_discharge` varchar(200) DEFAULT NULL, `export_invoice` varchar(45) DEFAULT NULL, `bol_note` varchar(45) DEFAULT NULL, `export_is_deleted` tinyint(1) DEFAULT '0', `created_by` int(11) DEFAULT NULL, `updated_by` int(11) DEFAULT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `legacy_customer_id` varchar(45) NOT NULL, `added_by_role` varchar(64) DEFAULT NULL, `customer_user_id` int(11) DEFAULT NULL, `notes_status` int(11) DEFAULT NULL, `oti_number` varchar(32) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select e.* from vehicle join vehicle_export ve on vehicle.id = ve.vehicle_id join export e on ve.export_id = e.id where location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796) group by e.id;";
    $insetSql = "INSERT INTO tmp_export (id, export_date, loading_date, broker_name, booking_number, eta, ar_number, xtn_number, seal_number, container_number, cutt_off, vessel, voyage, terminal, streamship_line, destination, itn, contact_details, special_instruction, container_type, port_of_loading, port_of_discharge, export_invoice, bol_note, export_is_deleted, created_by, updated_by, created_at, updated_at, legacy_customer_id, added_by_role, customer_user_id, notes_status, oti_number) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['export_date'], $row['loading_date'], $row['broker_name'], $row['booking_number'], $row['eta'], $row['ar_number'], $row['xtn_number'], $row['seal_number'], $row['container_number'], $row['cutt_off'], $row['vessel'], $row['voyage'], $row['terminal'], $row['streamship_line'], $row['destination'], $row['itn'], $row['contact_details'], $row['special_instruction'], $row['container_type'], $row['port_of_loading'], $row['port_of_discharge'], $row['export_invoice'], $row['bol_note'], $row['export_is_deleted'], $row['created_by'], $row['updated_by'], $row['created_at'], $row['updated_at'], $row['legacy_customer_id'], $row['added_by_role'], $row['customer_user_id'], $row['notes_status'], $row['oti_number']]);
    }
    echo "export data added to tmp_export table.\n";

    // Migrate VehicleExport data to tmp_vehicle_export table
    $connAsl->exec("ALTER TABLE vehicle_export MODIFY COLUMN vehicle_export_is_deleted tinyint(1) DEFAULT '0';");
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_vehicle_export`; CREATE TABLE `tmp_vehicle_export`( `id` int(11) NOT NULL AUTO_INCREMENT, `vehicle_id` int(11) NOT NULL, `export_id` int(11) NOT NULL, `customer_user_id` int(11) NOT NULL, `custom_duty` double DEFAULT NULL, `clearance` double DEFAULT NULL, `towing` double DEFAULT NULL, `shipping` double DEFAULT NULL, `storage` double DEFAULT NULL, `local` double DEFAULT NULL, `others` double DEFAULT NULL, `additional` double DEFAULT NULL, `vat` double DEFAULT NULL, `remarks` varchar(250) DEFAULT NULL, `title` double DEFAULT NULL, `discount` double DEFAULT NULL, `vehicle_export_is_deleted` tinyint(1) DEFAULT '0', `notes_status` int(1) DEFAULT 0, `exchange_rate` double DEFAULT NULL, `ocean_charges` double NOT NULL DEFAULT 0, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
    $sql = "select ve.* from vehicle join vehicle_export ve on vehicle.id = ve.vehicle_id where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_vehicle_export (id,vehicle_id,export_id,customer_user_id,custom_duty,clearance,towing,shipping,storage,local,others,additional,vat,remarks,title,discount,vehicle_export_is_deleted,notes_status,exchange_rate,ocean_charges) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['vehicle_id'], $row['export_id'], $row['customer_user_id'], $row['custom_duty'], $row['clearance'], $row['towing'], $row['shipping'], $row['storage'], $row['local'], $row['others'], $row['additional'], $row['vat'], $row['remarks'], $row['title'], $row['discount'], $row['vehicle_export_is_deleted'], $row['notes_status'], $row['exchange_rate'], $row['ocean_charges']]);
    }
    echo "vehicle_export data added to tmp_vehicle_export table.\n";

    // Migrate TowingRequest data to tmp_towing_request table
    $connAsl->exec("ALTER TABLE towing_request MODIFY COLUMN pictures tinyint(1) DEFAULT NULL;");
    $connAsl->exec("ALTER TABLE towing_request MODIFY COLUMN towed tinyint(1) DEFAULT NULL;");
    $connAsl->exec("ALTER TABLE towing_request MODIFY COLUMN title_recieved tinyint(1) DEFAULT NULL;");
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_towing_request`; CREATE TABLE `tmp_towing_request`( `id` int(11) NOT NULL AUTO_INCREMENT, `condition` int(11) DEFAULT NULL, `damaged` int(11) DEFAULT NULL, `pictures` tinyint(1) DEFAULT NULL, `towed` tinyint(1) DEFAULT NULL, `title_recieved` tinyint(1) DEFAULT NULL, `title_recieved_date` date DEFAULT NULL, `title_number` varchar(450) DEFAULT NULL, `title_state` varchar(450) DEFAULT NULL, `towing_request_date` date DEFAULT NULL, `pickup_date` date DEFAULT NULL, `deliver_date` date DEFAULT NULL, `note` text DEFAULT NULL, `title_type` int(11) DEFAULT 0, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select tr.* from vehicle join towing_request tr on vehicle.towing_request_id = tr.id where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_towing_request (`id`, `condition`, `damaged`, `pictures`, `towed`, `title_recieved`, `title_recieved_date`, `title_number`, `title_state`, `towing_request_date`, `pickup_date`, `deliver_date`, `note`, `title_type`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['condition'], $row['damaged'], $row['pictures'], $row['towed'], $row['title_recieved'], $row['title_recieved_date'], $row['title_number'], $row['title_state'], $row['towing_request_date'], $row['pickup_date'], $row['deliver_date'], $row['note'], $row['title_type']]);
    }
    echo "towing_request data added to tmp_towing_request table.\n";

    // Migrate Vehicle data to tmp_vehicle table
    $connAsl->exec("ALTER TABLE vehicle MODIFY COLUMN vehicle_is_deleted tinyint(1) DEFAULT '0';");
    $connAsl->exec("ALTER TABLE vehicle MODIFY COLUMN `keys` tinyint(1) DEFAULT NULL;");
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_vehicle`; CREATE TABLE `tmp_vehicle`( `id` int(11) NOT NULL AUTO_INCREMENT, `hat_number` varchar(45) DEFAULT NULL, `year` varchar(45) DEFAULT NULL, `color` varchar(45) DEFAULT NULL, `model` varchar(45) DEFAULT NULL, `make` varchar(45) DEFAULT NULL, `vin` varchar(45) DEFAULT NULL, `weight` varchar(45) DEFAULT NULL, `pieces` varchar(45) DEFAULT NULL, `value` varchar(45) DEFAULT NULL, `license_number` varchar(45) DEFAULT NULL, `towed_from` varchar(256) DEFAULT NULL, `lot_number` varchar(45) DEFAULT NULL, `towed_amount` double DEFAULT NULL, `storage_amount` varchar(256) DEFAULT NULL, `status` int(11) DEFAULT NULL, `check_number` varchar(32) DEFAULT NULL, `additional_charges` double DEFAULT NULL, `location` varchar(256) DEFAULT NULL, `customer_user_id` int(11) NOT NULL, `towing_request_id` int(11) NOT NULL, `created_at` timestamp NULL DEFAULT NULL, `updated_at` timestamp NULL DEFAULT NULL, `created_by` int(11) NOT NULL, `updated_by` int(11) NOT NULL, `is_export` int(1) DEFAULT NULL, `title_amount` varchar(45) DEFAULT NULL, `container_number` varchar(45) DEFAULT NULL, `keys` tinyint(1) DEFAULT NULL, `vehicle_is_deleted` tinyint(1) DEFAULT '0', `notes_status` int(1) DEFAULT 0, `added_by_role` varchar(64) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select vehicle.* from vehicle where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_vehicle (`id`, `hat_number`, `year`, `color`, `model`, `make`, `vin`, `weight`, `pieces`, `value`, `license_number`, `towed_from`, `lot_number`, `towed_amount`, `storage_amount`, `status`, `check_number`, `additional_charges`, `location`, `customer_user_id`, `towing_request_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `is_export`, `title_amount`, `container_number`, `keys`, `vehicle_is_deleted`, `notes_status`, `added_by_role`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['hat_number'], $row['year'], $row['color'], $row['model'], $row['make'], $row['vin'], $row['weight'], $row['pieces'], $row['value'], $row['license_number'], $row['towed_from'], $row['lot_number'], $row['towed_amount'], $row['storage_amount'], $row['status'], $row['check_number'], $row['additional_charges'], $row['location'], $row['customer_user_id'], $row['towing_request_id'], $row['created_at'], $row['updated_at'], $row['created_by'], $row['updated_by'], $row['is_export'], $row['title_amount'], $row['container_number'], $row['keys'], $row['vehicle_is_deleted'], $row['notes_status'], $row['added_by_role']]);
    }
    echo "vehicle data added to tmp_vehicle table.\n";

    // Migrate features to tmp_features table
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_features`; CREATE TABLE `tmp_features`( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(45) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = 'SELECT * FROM `features`;';
    $insetSql = "INSERT INTO tmp_features (id, name) VALUES (?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['name']]);
    }
    $connAsl->exec("UPDATE `tmp_features` LEFT JOIN `features` ON `tmp_features`.name = `features`.name SET `tmp_features`.asl_id = `features`.id;");
    echo "features data added to tmp_features table.\n";

    // Migrate vehicle_features to tmp_vehicle_features
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_vehicle_features`; CREATE TABLE `tmp_vehicle_features`( `id` int(11) NOT NULL AUTO_INCREMENT, `vehicle_id` int(11) NOT NULL, `features_id` int(11) NOT NULL, `value` int(11) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select vf.* from vehicle join vehicle_features vf on vf.vehicle_id = vehicle.id where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_vehicle_features (`id`, `vehicle_id`, `features_id`, `value`) VALUES (?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['vehicle_id'], $row['features_id'], $row['value']]);
    }
    echo "vehicle_features data added to tmp_vehicle_features table.\n";

    // Migrate images to tmp_images
    $connAsl->exec("DROP TABLE IF EXISTS `tmp_images`; CREATE TABLE `tmp_images`( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(128) DEFAULT NULL, `thumbnail` varchar(128) DEFAULT NULL, `normal` varchar(45) DEFAULT NULL, `vehicle_id` int(11) NOT NULL, `is_deleted` bit(1) DEFAULT NULL, `created_at` timestamp NULL DEFAULT current_timestamp(), `updated_at` timestamp NULL DEFAULT NULL, `created_by` int(11) DEFAULT NULL, `updated_by` int(11) DEFAULT NULL, `baseurl` varchar(450) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select images.* from vehicle join images on images.vehicle_id = vehicle.id where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_images (`id`, `name`, `thumbnail`, `normal`, `vehicle_id`, `is_deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`, `baseurl`) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
    $stmt= $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['name'], $row['thumbnail'], $row['normal'], $row['vehicle_id'], $row['is_deleted'], $row['created_at'], $row['updated_at'], $row['created_by'], $row['updated_by'], $row['baseurl']]);
    }
    echo "images data added to tmp_images table.\n";

    // Migrate document to tmp_documents
    $connAsl->exec("DROP TABLE IF EXISTS tmp_documents; CREATE TABLE `tmp_documents`( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(256) DEFAULT NULL, `thumbnail` varchar(256) DEFAULT NULL, `normal` varchar(45) DEFAULT NULL, `is_deleted` bit(1) DEFAULT NULL, `created_at` timestamp NULL DEFAULT current_timestamp(), `updated_at` timestamp NULL DEFAULT NULL, `created_by` int(11) DEFAULT NULL, `updated_by` int(11) DEFAULT NULL, `vehicle_id` int(11) NOT NULL, `invoice_id` int(11) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    $sql = "select d.* from vehicle join documents d on vehicle.id = d.vehicle_id where vehicle.location in(5, 6) AND d.vehicle_id IS NOT NULL AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796);";
    $insetSql = "INSERT INTO tmp_documents (`id`, `name`, `thumbnail`, `normal`, `is_deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`, `vehicle_id`, `invoice_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    $stmt = $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['name'], $row['thumbnail'], $row['normal'], $row['is_deleted'], $row['created_at'], $row['updated_at'], $row['created_by'], $row['updated_by'], $row['vehicle_id'], $row['invoice_id']]);
    }
    echo "documents data added to tmp_documents table.\n";

    // Migrate export_images to tmp_export_images table
    $connAsl->exec("DROP TABLE IF EXISTS tmp_export_images; CREATE TABLE `tmp_export_images`( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(45) DEFAULT NULL, `thumbnail` varchar(45) DEFAULT NULL, `export_id` int(11) NOT NULL, `baseurl` varchar(450) DEFAULT NULL, `asl_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
    $sql = "select ei.* from vehicle join vehicle_export ve on vehicle.id = ve.vehicle_id join export e on ve.export_id = e.id join export_images ei on e.id = ei.export_id where vehicle.location in(5, 6) AND vehicle.customer_user_id in (7000355, 7000360, 900511, 7000313, 7000363, 7000365, 7000359, 900855, 900711, 900737, 900796) GROUP BY ei.id;";
    $insetSql = "INSERT INTO tmp_export_images (`id`, `name`, `thumbnail`, `export_id`, `baseurl`) VALUES (?,?,?,?,?)";
    $stmt = $connAsl->prepare($insetSql);
    foreach ($connAriana->query($sql) as $row) {
        $stmt->execute([$row['id'], $row['name'], $row['thumbnail'], $row['export_id'], $row['baseurl']]);
    }
    echo "export_images data added to tmp_export_images table.\n";
} catch (Exception $e) {
    echo $e->getMessage();
}


?>

